# encoding=utf-8
from flask import jsonify
from app.common.error_code import ErrorCode

def success_result(data={}, page={}):
    ret = {
        'code': 0,
        **({'data': data} if data else {}),
        **({'page': page} if page else {})
    }
    return jsonify(ret)


def error_result(error=ErrorCode.ERROR_UNKNOWN, message="", data={}, with_code=False):
    code, message = error
    ret = {
        'code': -code,
        **({'data': data} if data else {}),
        'message': "u%s (error: %d)"%(message, code) if with_code else message
    }
    return jsonify(ret)
