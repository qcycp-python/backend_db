# encoding=utf-8
from flask_sqlalchemy import SQLAlchemy
from app.common.logger import Logger

db = SQLAlchemy()
logger = Logger("sample").get_logger()
