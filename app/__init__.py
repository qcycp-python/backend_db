from flask import Flask
from app import views
from app.foundation import db, logger

def create_app():
    app = Flask(__name__)
    app.config.from_object('config')

    configure_blueprint(app, views.MODULES)
    configure_foundations(app)

    return app

def configure_blueprint(app, modules):
    for module in modules:
        app.register_blueprint(module)

def configure_foundations(app):
    db.app = app
    db.init_app(app)

    with app.app_context():
        from app.models import Sample
        db.create_all()
