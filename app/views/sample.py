from flask import Blueprint
from app.common.json_builder import success_result, error_result

sample_bp = Blueprint('sample_bp', __name__, url_prefix='/api')

@sample_bp.route("/test", methods=['GET'])
def test():
    return success_result("Hello World!!")
