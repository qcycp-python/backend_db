import logging
import os
from logging.handlers import RotatingFileHandler
from logging.handlers import TimedRotatingFileHandler
from config import LOGDIR

class Logger(object):
    def __init__(self, _name):
        formatter = logging.Formatter('%(asctime)s - [%(filename)s:%(lineno)s] - %(levelname)s %(message)s ')
        self.__logger = logging.getLogger(_name)
        self.__logger.setLevel(logging.DEBUG)

        if not os.path.exists(LOGDIR):
            os.mkdir(LOGDIR)
        fileHandler = TimedRotatingFileHandler(os.path.join(LOGDIR, _name+'.log'), when="h", interval=1, backupCount=48, encoding='utf-8')
        fileHandler.setFormatter(formatter)

        streamHandler = logging.StreamHandler()
        streamHandler.setFormatter(formatter)

        self.__logger.addHandler(fileHandler)
        self.__logger.addHandler(streamHandler)

    def get_logger(self):
        return self.__logger
