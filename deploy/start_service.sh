#!/bin/bash

echo "Starting mysql..."
supervisorctl start mysql

sleep 3

echo "Starting gunicorn..."
supervisorctl start gunicorn
