# -*- coding: utf-8 -*-
import json
import os
from app.foundation import logger
from test import test_app

class TestCamera(object):
    @classmethod
    def setup_class(self):
        logger.info('\nsetup_class: %s' % os.path.basename(__file__))

    @classmethod
    def teardown_class(self):
        logger.info('\nteardown_class: %s' % os.path.basename(__file__))

    def test_index(self):
        ret = test_app.get('/api/test')
        assert ret.status_code == 200
