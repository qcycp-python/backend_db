This is a sample backend project, including mysql inside the same docker container.  
The Flask application is deployed on nginx and guniorn.  
And the services is started by supervisor.  

## How to run in develop environment  
virtualenv venv  
source venv/bin/activate  
pip install -r requirements.txt  
export FLASK_APP=main.py  
flask run --host 0.0.0.0 --port 7777  

## How to deploy in local vm  
cd deploy  
sh deploy.sh  

## How to formal release  
cd deploy  
make  

#### Release files  
##### All files would be stored in deploy/Multisite-HeadQuarter, including  
1. docker-compose.yml  
2. deploy_sample.A0.0.0.abcdefg.tar  

##### All files would be zip in Multisite-HeadQuarter.A0.0.0.abcdefg.tar.xz  
You can unzip the file by:  
```
tar Jxvf Sample.A0.0.0.abcdefg.tar.xz
```

### mysql port will listen on port 7778  
Persistent data of mysql would be stored at /opt/docker-software/sample  
