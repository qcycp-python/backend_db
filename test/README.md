##### Execute command in project root folder /app:  
python3.6 -m pytest -vv -s test --cov=. --cov-config=test/setup.cfg --cov-report=html --html=coverage/log.html

##### Coverage report:  
/app/coverage/index.html

##### Test report:  
/app/coverage/log.html

##### Copy reports to local vm from docker:  
docker cp hh_sample:/app/coverage .
