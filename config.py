import os

BASEDIR = os.path.abspath(os.path.dirname(__file__))
LOGDIR = os.path.join(BASEDIR, "logs")

SECRET_KEY = os.environ.get('SECRET_KEY') or 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
SQLALCHEMY_TRACK_MODIFICATIONS = False

MYSQL_DATABASE = os.environ.get('MYSQL_DATABASE') or 'error'
MYSQL_ROOT_PASSWORD = os.environ.get('MYSQL_ROOT_PASSWORD') or 'error'
SQLALCHEMY_DATABASE_URI = 'mysql://{0}:{1}@{2}/{3}'.format('root', MYSQL_ROOT_PASSWORD, 'localhost', MYSQL_DATABASE)
