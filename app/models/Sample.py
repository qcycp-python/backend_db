# encoding=utf-8
from app.foundation import db

class Sample(db.Model):
    __tablename__ = 'Sample'
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(64))

    def __init__(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)

    def __repr__(self):
        return '<Sample {} {}>'.format(self.id, self.description)
    
    def get_json(self):
        info = {
            'id': self.id,
            'description': self.description
        }

        return info
